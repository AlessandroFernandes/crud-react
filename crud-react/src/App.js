import React, { Component } from 'react';

import './App.css';


class App extends React.Component {

  constructor() {
    super();
    this.state = {
      title: "React Simple CRUD application",
      act: 0,
      index: '',
      datas: []
    }
  }

  componenteTeste = () => {
    this.refs.name.focus();
  }


  fSubmit = (e) => {
    e.preventDefault();

    let datas = this.state.datas;
    let name = this.refs.name.value;
    let andress = this.refs.andress.value;

    if (this.state.act === 0) { //new

      let data = {
        name, andress
      }
      datas.push(data);

    } else { //update

      let index = this.state.index;
      datas[index].name = name;
      datas[index].andress = andress;

    }

    this.setState({
      datas: datas,
      act: 0
    })

    this.refs.myForm.reset();
    this.refs.name.focus();

  }
  deletar = (i) => {

    let data = this.state.datas;
    data.splice(i, 1);
    this.setState({
      datas: data
    })
    this.refs.name.focus();

  }
  editar = (i) => {

    let data = this.state.datas[i];
    this.refs.name.value = data.name;
    this.refs.andress.value = data.andress;

    this.setState({
      act: 1,
      index: i

    })
    this.refs.name.focus();

  }


  render() {
    return (
      <div className="App">
        <h2>{this.state.title}</h2>

        <form ref="myForm" className="myForm">
          <input type="text" ref="name" placeholder="your name" className="formField" />
          <input type="text" ref="andress" placeholder="your andress" className="formField" />
          <button onClick={(e) => this.fSubmit(e)} className="myButton">Submit</button>
        </form>

        <pre>
          {this.state.datas.map((index, i) =>
            <li key={i} className="myList">{i + 1}. {index.name}, {index.andress}-
               <button onClick={() => this.deletar(i)} className="delete">Delete</button>-
               <button onClick={() => this.editar(i)} className="edit">Edit</button>
            </li>
          )}
        </pre>

      </div>
    );
  }
}

export default App;
